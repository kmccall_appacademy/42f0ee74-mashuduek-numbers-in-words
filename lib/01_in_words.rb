require 'byebug'
class Fixnum
  ONES = {
        0 => 'zero',
        1 => 'one',
        2 => 'two',
        3 => 'three',
        4 => 'four',
        5 => 'five',
        6 => 'six',
        7 => 'seven',
        8 => 'eight',
        9 => 'nine'
  }

  TEENS = {
        10 => 'ten',
        11 => 'eleven',
        12 => 'twelve',
        13 => 'thirteen',
        14 => 'fourteen',
        15 => 'fifteen',
        16 => 'sixteen',
        17 => 'seventeen',
        18 => 'eighteen',
        19 => 'nineteen'
  }

  TENS = {
        20 => 'twenty',
        30 => 'thirty',
        40 => 'forty',
        50 => 'fifty',
        60 => 'sixty',
        70 => 'seventy',
        80 => 'eighty',
        90 => 'ninety'
  }

  def in_words
      number = self
      return number.under_k if number < 1000

      under_quad = []
      while number >= 1_000_000_000_000
        under_quad << (number / 1_000_000_000_000).under_k << 'trillion'
        number %= 1_000_000_000_000
      end

      under_tril = []
      while number >= 1_000_000_000
        under_tril << (number / 1_000_000_000).under_k << 'billion'
        number %= 1_000_000_000
      end

      under_bil = []
      while number >= 1_000_000
        under_bil << (number / 1_000_000).under_k << 'million'
        number %= 1_000_000
      end

      under_mil = []
      while number >= 1000
        under_mil << (number / 1000).under_k << 'thousand'
        number %= 1000
      end

      under_thou = []
      while number > 0
        under_thou << (number % 1000).under_k
        number /= 1000
      end

    to_return = under_thou << under_mil << under_bil << under_tril << under_quad
    to_return.reverse.flatten.join(' ')

    #first version BELOW --- new version ABOVE

    # debugger
    # if self < 100 # UNDER HUNDRED
    #   return (self % 100).under_hundred
    # elsif self.between?(100, 999) # UNDER THOUSAND
    #   to_return = self.under_k
    #   to_return << ' ' + (self % 100).under_hundred unless (self % 100) == 0
    # elsif self.between?(1000, 999_999) # UNDER MILLION
    #
    #   if self/1000 < 100
    #     to_return << (self/1000).under_hundred + ' thousand'
    #     to_return << ' ' + (self % 1000).under_k unless (self % 1000) == 0
    #   else
    #     to_return << (self/1000).under_k << ' thousand'
    #     to_return << ' ' + (self % 1000).under_k unless (self % 1000) == 0
    #   end
    #
    # elsif self.between?(1_000_000, 999_999_999) # UNDER BILLION
    #   to_return << (self / 1_000_000).under_k + ' million'
    #   unless self % 1_000_000 == 0
    #
    #     unless (self % 1_000_000 / 1000) == 0 # ENSURING THOUSANDS IS NOT 0
    #       to_return << ' ' + (self % 1_000_000 / 1000).under_k + ' thousand'
    #     end
    #
    #     unless self % 1000 == 0
    #       to_return << ' ' + (self % 1000).under_k
    #     end
    #   end
    #
    # elsif self.between?(1_000_000_000, 999_999_999_999) # UNDER TRILLION
    #   to_return << (self / 1_000_000_000).under_k + ' billion'
    #   unless self % 1_000_000_000 == 0
    #
    #     unless (self % 1_000_000_000 / 1_000_000) == 0
    #       to_return << ' ' << (self % 1_000_000_000 / 1_000_000).under_k + ' million'
    #     end
    #
    #     unless (self % 1_000_000 / 1000) == 0
    #       to_return << ' ' << (self % 1_000_000 / 1000).under_k + ' thousand'
    #     end
    #
    #     unless (self % 1000) == 0
    #       to_return << ' ' + (self % 1000).under_k
    #     end
    #   end
    # elsif self.between?(1_000_000_000_000, 999_999_999_999_999) # UNDER QUAD
    #   to_return << (self/1_000_000_000_000).under_k + ' trillion'
    #   unless self % 1_000_000_000_000 == 0
    #
    #     unless (self % 1_000_000_000_000 / 1_000_000_000) == 0
    #       to_return << ' ' << (self % 1_000_000_000_000 / 1_000_000_000).under_k + ' billion'
    #     end
    #
    #     unless (self % 1_000_000_000 / 1_000_000) == 0
    #       to_return << ' ' << (self % 1_000_000_000 / 1_000_000).under_k + ' million'
    #     end
    #
    #     unless (self % 1_000_000 / 1000) == 0
    #       to_return << ' ' << (self % 1_000_000 / 1000).under_k + ' thousand'
    #     end
    #
    #     unless self % 1000 == 0
    #       to_return << ' ' << (self % 1000).under_k
    #     end
    #   end
    # end
    # to_return

  end

  def under_hundred
    if self < 10
      return ONES[self]
    elsif self > 19
      to_return = ''
      if self % 10 != 0
        to_return << TENS[self - (self%10)] << ' ' << ONES[self % 10]
      else
        to_return << TENS[self]
      end
    elsif self.between?(10, 19)
      return TEENS[self]
    end
    to_return
  end

  def under_k
    to_retrun = ''
    if self % 1000 < 100
      to_retrun << self.under_hundred
    else
      to_retrun << (ONES[self/100] + ' hundred')
      unless (self % 100) == 0
        to_retrun << ' ' << (self % 100).under_hundred
      end
    end
    to_retrun   # WHATS DIFFERENCE BETWEEN << AND + ???
  end

end
